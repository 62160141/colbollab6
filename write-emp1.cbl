       IDENTIFICATION DIVISION. 
       PROGRAM-ID. WRITE-EMP.
       AUTHOR. RENU.

       ENVIRONMENT DIVISION. 
       INPUT-OUTPUT SECTION. 
       FILE-CONTROL. 
           SELECT EMP-FILE ASSIGN TO "emp1.dat"
              ORGANIZATION IS LINE SEQUENTIAL.

       DATA DIVISION. 
       FILE SECTION.
       FD EMP-FILE.
       01  EMP-DETIALS.
           88 END-OF-EMP-FILE   VALUE HIGH-VALUE.
           05 EMP-SSN PIC 9(9).
           05 EMP-MAME.
              10 EMP-SURNAME PIC X(15).
              10 EMP-FORENAME PIC X(10).
           05 EMP-DATE-OF-BIRTH.
              10 EMP-YOB PIC 9(4).
              10 EMP-MOB PIC 9(2).
              10 EMP-DOB PIC X.
           05 EMP-GENDER PIC X.

       PROCEDURE DIVISION.
       BEGIN.
           OPEN OUTPUT EMP-FILE 
           MOVE "123456789" TO EMP-SSN
           MOVE "RENU" TO EMP-SURNAME 
           MOVE "PARAGUN" TO EMP-FORENAME
           MOVE "14111999"  TO EMP-DATE-OF-BIRTH 
           MOVE "F"         TO EMP-GENDER 
           WRITE EMP-DETIALS 

           MOVE "987654321" TO EMP-SSN
           MOVE "RRRRR" TO EMP-SURNAME 
           MOVE "POAKL" TO EMP-FORENAME
           MOVE "19997524"  TO EMP-DATE-OF-BIRTH 
           MOVE "M"         TO EMP-GENDER 
           WRITE EMP-DETIALS

           MOVE "1122334455" TO EMP-SSN
           MOVE "HAHAHA" TO EMP-SURNAME 
           MOVE "POOOOO" TO EMP-FORENAME
           MOVE "875342"  TO EMP-DATE-OF-BIRTH 
           MOVE "M"         TO EMP-GENDER 
           WRITE EMP-DETIALS

           CLOSE EMP-FILE 
           GOBACK 
           .

